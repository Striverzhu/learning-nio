package com.hang.file.stream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;

public class TestIO {
    public static void main(String[] args) {
        method();
    }

    public static void method() {
        InputStream in = null;
        try {
            in = new BufferedInputStream(new FileInputStream("E:\\learing-java\\test.txt"));
            byte[] buf = new byte[1024];
            int bytesRead = in.read(buf);
            while (bytesRead != -1) {
                for (int i = 0; i < bytesRead; i++) {
                    System.out.println((char) buf[i]);
                }
                bytesRead = in.read(buf);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
