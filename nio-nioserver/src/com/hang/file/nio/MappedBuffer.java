package com.hang.file.nio;

import sun.rmi.runtime.Log;

import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class MappedBuffer {
    public static void main(String[] args) {
        method();
    }

    public static void method() {
        RandomAccessFile accessFile = null;
        FileChannel fileChannel = null;
        try {
            accessFile = new RandomAccessFile("D:\\WeChatSetup.exe", "rw");
            fileChannel = accessFile.getChannel();
            long begin = System.currentTimeMillis();
            MappedByteBuffer mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, (long) accessFile.length());
            long end = System.currentTimeMillis();
            System.out.println("time used : " + (end - begin));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (accessFile != null) {
                    accessFile.close();
                }
                if (fileChannel != null) {
                    fileChannel.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
