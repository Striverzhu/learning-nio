package com.hang.file.nio;

import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Buffer {
    public static void main(String[] args) {
        method();
    }

    public static void method() {
        RandomAccessFile randomAccessFile = null;
        FileChannel fileChannel = null;
        try {
            randomAccessFile = new RandomAccessFile("D:\\WeChatSetup.exe", "rw");
            fileChannel = randomAccessFile.getChannel();
            long begin = System.currentTimeMillis();
            ByteBuffer buffer = ByteBuffer.allocate((int) randomAccessFile.length());
            buffer.clear();
            fileChannel.read(buffer);
            long end = System.currentTimeMillis();
            System.out.println("time used : " + (end - begin));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (randomAccessFile != null) {
                    randomAccessFile.close();
                }
                if (fileChannel != null) {
                    fileChannel.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
