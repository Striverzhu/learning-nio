package com.hang.pipe;

import java.nio.ByteBuffer;
import java.nio.channels.Pipe;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class test {
    public static void main(String[] args) {
        Pipe pipe;
        ExecutorService service = Executors.newFixedThreadPool(2);
        try {
            pipe = Pipe.open();
            final Pipe tempPipe = pipe;
            service.submit(() -> {
                Pipe.SinkChannel sinkChannel = tempPipe.sink();
                while (true) {
                    TimeUnit.SECONDS.sleep(1);
                    String data = "Pipe test at time +" + System.currentTimeMillis();
                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    buffer.clear();
                    buffer.put(data.getBytes());
                    buffer.flip();
                    while (buffer.hasRemaining()) {
                        System.out.println(buffer);
                        sinkChannel.write(buffer);
                    }
                }
            });
            service.submit(() -> {
                Pipe.SourceChannel sourceChannel = tempPipe.source();
                while (true) {
                    ByteBuffer buffer = ByteBuffer.allocate(1024);
                    buffer.clear();
                    int bytesRead = sourceChannel.read(buffer);
                    System.out.println("bytesRead=" + bytesRead);
                    while (bytesRead > 0) {
                        buffer.flip();
                        byte[] b = new byte[bytesRead];
                        int i = 0;
                        while (buffer.hasRemaining()) {
                            b[i] = buffer.get();
                            System.out.printf("%x", b[i]);
                            i++;
                        }
                        String s = new String(b);
                        System.out.println("=============||" + s);
                        bytesRead = sourceChannel.read(buffer);
                    }
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            service.shutdown();
        }
    }
}
