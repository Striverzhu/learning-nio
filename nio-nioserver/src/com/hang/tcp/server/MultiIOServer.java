package com.hang.tcp.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by striver_zhu on 2018/3/27.
 */
public class MultiIOServer {
    private static ExecutorService executorService = Executors.newFixedThreadPool(3);

    public static void main(String[] args) {
        try {
            ServerSocket server = new ServerSocket(8686);       //创建一个服务端且端口为8686
            Socket client = null;
            while (true) {           //循环监听
                client = server.accept();       //服务端监听到一个客户端请求
                System.out.println(client.getRemoteSocketAddress() + "地址的客户端连接成功!");
                executorService.submit(new HandleMsg(client));      //将该客户端请求通过线程池放入HandlMsg线程中进行处理
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static class HandleMsg implements Runnable {
        private Socket client;

        public HandleMsg(Socket client) {
            this.client = client;
        }

        public void run() {
            BufferedReader bufferedReader = null;       //创建字符缓存输入流
            PrintWriter printWriter = null;         //创建字符写入流
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));        //获取客户端的输入流
                printWriter = new PrintWriter(client.getOutputStream(), true);            //获取客户端的输出流，true是随时刷新
                String inputLine = null;
                long a = System.currentTimeMillis();
                while ((inputLine = bufferedReader.readLine()) != null) {
                    printWriter.println(inputLine);
                }
                long b = System.currentTimeMillis();
                System.out.println("此线程花费了：" + (b - a) + "秒！");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bufferedReader.close();
                    printWriter.close();
                    client.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
