package com.hang.tcp.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeUnit;

public class NIOClient {
    public static void main(String[] args){
        client();
    }

    public static void client(){
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        SocketChannel socketChannel = null;
        try {
            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);
            socketChannel.connect(new InetSocketAddress("127.0.0.1",8686));
            if (socketChannel.finishConnect()){
                while (true){
                    TimeUnit.SECONDS.sleep(1);
                    String info = "the info from client";
                    buffer.clear();
                    buffer.put(info.getBytes());
                    buffer.flip();
                    while (buffer.hasRemaining()){
                        System.out.println(buffer);
                        socketChannel.write(buffer);
                    }
                }
            }
        }catch (IOException | InterruptedException ex){
            ex.printStackTrace();
        }finally {
            try {
                if (socketChannel !=null){
                    socketChannel.close();
                }
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }
}
